package com.upgrade.campsite.util;

import com.upgrade.campsite.model.Reservation;

import java.time.LocalDate;

public final class EntityBuilderUtils {

    public static Reservation buildReservation() {
        return Reservation.builder()
                .email("diogocarleto@gmail.com")
                .name("Diogo Carleto")
                .arrivalAt(LocalDate.now().minusDays(1))
                .departureAt(LocalDate.now().plusDays(2))
                .build();
    }
}
