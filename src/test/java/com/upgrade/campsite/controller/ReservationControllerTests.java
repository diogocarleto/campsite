package com.upgrade.campsite.controller;

import com.upgrade.campsite.CampsiteApplication;
import com.upgrade.campsite.exception.ErrorMessageEnum;
import com.upgrade.campsite.exception.ErrorMessageInfo;
import com.upgrade.campsite.model.Reservation;
import com.upgrade.campsite.repository.ReservationRepository;
import com.upgrade.campsite.util.EntityBuilderUtils;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CampsiteApplication.class, webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReservationControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ReservationRepository reservationRepository;

    @After
    public void afterTests() {
        reservationRepository.deleteAll();
    }

    @Test
    public void reserve() {
        Reservation reservation = EntityBuilderUtils.buildReservation();
        Reservation reservationDb = restTemplate.postForEntity("/reservation/reserve", reservation, Reservation.class).getBody();
        assertThat(reservationDb, notNullValue());
        assertThat(reservationDb.getId(), notNullValue());
    }

    @Test
    public void reserveNotAvailable() {
        Reservation reservation = EntityBuilderUtils.buildReservation();
        Reservation reservationDb = restTemplate.postForEntity("/reservation/reserve", reservation, Reservation.class).getBody();
        assertThat(reservationDb, notNullValue());
        assertThat(reservationDb.getId(), notNullValue());

        ErrorMessageInfo errorMessageInfo = restTemplate.postForEntity("/reservation/reserve", reservation, ErrorMessageInfo.class).getBody();
        assertThat(errorMessageInfo.getCode(), is(ErrorMessageEnum.CAMPSITE_NOT_AVAILABLE.code));
    }

    @Test
    public void reserveMoreThan3Days() {
        Reservation reservation = Reservation.builder()
                .email("diogocarleto@gmail.com")
                .name("Diogo Carleto")
                .arrivalAt(LocalDate.now())
                .departureAt(LocalDate.now().plusDays(5))
                .build();
        ErrorMessageInfo errorMessageInfo = restTemplate.postForEntity("/reservation/reserve", reservation, ErrorMessageInfo.class).getBody();
        assertThat(errorMessageInfo.getCode(), is(ErrorMessageEnum.MAX_3_DAYS_HOSTED.code));
    }

    @Test
    public void reserveCheckInOutSameDay() {
        Reservation reservation = Reservation.builder()
                .email("diogocarleto@gmail.com")
                .name("Diogo Carleto")
                .arrivalAt(LocalDate.now())
                .departureAt(LocalDate.now())
                .build();
        ErrorMessageInfo errorMessageInfo = restTemplate.postForEntity("/reservation/reserve", reservation, ErrorMessageInfo.class).getBody();
        assertThat(errorMessageInfo.getCode(), is(ErrorMessageEnum.MIN_1_DAY_HOSTED.code));
    }

    @Test
    public void reserveTodayForCheckInToday() {
        Reservation reservation = Reservation.builder()
                .email("diogocarleto@gmail.com")
                .name("Diogo Carleto")
                .arrivalAt(LocalDate.now())
                .departureAt(LocalDate.now().plusDays(2))
                .build();
        ErrorMessageInfo errorMessageInfo = restTemplate.postForEntity("/reservation/reserve", reservation, ErrorMessageInfo.class).getBody();
        assertThat(errorMessageInfo.getCode(), is(ErrorMessageEnum.MIN_1_BEFORE_DAY_ARRIVAL.code));
    }

    @Test
    public void reserveMoreThan1Month() {
        Reservation reservation = Reservation.builder()
                .email("diogocarleto@gmail.com")
                .name("Diogo Carleto")
                .arrivalAt(LocalDate.now().minusMonths(2))
                .departureAt(LocalDate.now().plusDays(2))
                .build();
        ErrorMessageInfo errorMessageInfo = restTemplate.postForEntity("/reservation/reserve", reservation, ErrorMessageInfo.class).getBody();
        assertThat(errorMessageInfo.getCode(), is(ErrorMessageEnum.MAX_1_MONTH_BEFORE_DAY_ARRIVAL.code));
    }

    @Test
    public void updateReservation() {
        Reservation reservation = EntityBuilderUtils.buildReservation();
        Reservation reservationDb = restTemplate.postForEntity("/reservation/reserve", reservation, Reservation.class).getBody();
        assertThat(reservationDb, notNullValue());
        assertThat(reservationDb.getId(), notNullValue());

        String emailChanged = "diogocarleto2@gmail.com";
        String nameChanged = "diogo2";
        Reservation reservationUpdated = reservationDb.clone();
        reservationUpdated.setEmail(emailChanged);
        reservationUpdated.setName(nameChanged);
        Reservation reservationUpdatedDb2 = restTemplate.exchange("/reservation/reserve", HttpMethod.PUT, new HttpEntity<>(reservationUpdated, null), Reservation.class).getBody();
        assertThat(reservationUpdatedDb2, notNullValue());
        assertThat(reservationUpdatedDb2.getId(), is(reservationDb.getId()));
        assertThat(reservationUpdatedDb2.getEmail(), is(emailChanged));
        assertThat(reservationUpdatedDb2.getName(), is(nameChanged));
    }

    @Test
    public void showAvailability() {
        List<LocalDate> availableDateList = restTemplate.exchange("/reservation/availability?beginDate={beginDate}&endDate={endDate}",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<LocalDate>>(){},
                LocalDate.now().withDayOfMonth(1), LocalDate.now().withDayOfMonth(1).plusDays(15))
                .getBody();

        assertThat(availableDateList.size(), is(15));

        Reservation reservation = EntityBuilderUtils.buildReservation();
        Reservation reservationDb = restTemplate.postForEntity("/reservation/reserve", reservation, Reservation.class).getBody();

        availableDateList = restTemplate.exchange("/reservation/availability?beginDate={beginDate}&endDate={endDate}",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<LocalDate>>(){},
                LocalDate.now().withDayOfMonth(1), LocalDate.now().withDayOfMonth(1).plusDays(15))
                .getBody();

        assertThat(availableDateList.size(), is(12));
    }
}
