package com.upgrade.campsite.service;

import com.upgrade.campsite.CampsiteApplication;
import com.upgrade.campsite.model.Reservation;
import com.upgrade.campsite.repository.ReservationRepository;
import com.upgrade.campsite.util.EntityBuilderUtils;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CampsiteApplication.class, webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReservationServiceTests {

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private ReservationRepository reservationRepository;

    @After
    public void afterTest() {
        reservationRepository.deleteAll();
    }

    @Test
    public void cancelReservation() {
        Reservation reservation = EntityBuilderUtils.buildReservation();
        Reservation reservationDb = reservationService.reserve(reservation);
        assertThat(reservationDb, notNullValue());
        assertThat(reservationDb.getId(), notNullValue());

        reservationService.cancelReservation(reservationDb.getId());
        List<Reservation> reservationList = new ArrayList<>();
        reservationRepository.findAll().forEach(reservationList::add);
        assertThat(reservationList.size(), is(0));
    }

    @Test
    public void getAvailableDaysPerPeriod() {
        reservationService.reserve(EntityBuilderUtils.buildReservation());
        reservationService.getAvailableDaysPerPeriod(LocalDate.now(), LocalDate.now().plusDays(15));
    }
}
