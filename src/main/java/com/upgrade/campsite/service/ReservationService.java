package com.upgrade.campsite.service;

import com.upgrade.campsite.exception.CampsiteException;
import com.upgrade.campsite.exception.ErrorMessageEnum;
import com.upgrade.campsite.model.Reservation;
import com.upgrade.campsite.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;

    public Reservation reserve(final Reservation reservation) {
        checkCheckInOutSameDay(reservation);
        checkIs30DaysBeforeArrival(reservation);
        checkIsExceedingNumDaysHosted(reservation);
        checkIsOneDayBeforeArrival(reservation);
        if (reservation.getId() == null && !isDataRangeFree(reservation)) {
            throw new CampsiteException(ErrorMessageEnum.CAMPSITE_NOT_AVAILABLE);
        }
        return reservationRepository.save(reservation);
    }

    public boolean cancelReservation(Long id) {
        reservationRepository.deleteById(id);
        return true;
    }

    public Reservation modifyReservation(final Reservation reservation) {
        return reservationRepository.save(reservation);
    }

    private void checkIsExceedingNumDaysHosted(final Reservation reservation) {
        if (getLocalDates(reservation.getArrivalAt(), reservation.getDepartureAt()).size() > 3) {
            throw new CampsiteException(ErrorMessageEnum.MAX_3_DAYS_HOSTED);
        }
    }

    private void checkCheckInOutSameDay(final Reservation reservation) {
        if (reservation.getArrivalAt().compareTo(reservation.getDepartureAt()) == 0) {
            throw new CampsiteException(ErrorMessageEnum.MIN_1_DAY_HOSTED);
        }
    }

    private void checkIsOneDayBeforeArrival(final Reservation reservation) {
        if (reservation.getArrivalAt().compareTo(LocalDate.now()) > -1) {
            throw new CampsiteException(ErrorMessageEnum.MIN_1_BEFORE_DAY_ARRIVAL);
        }
    }

    private void checkIs30DaysBeforeArrival(final Reservation reservation) {
        if (reservation.getArrivalAt().compareTo(LocalDate.now().minusMonths(1)) < 1) {
            throw new CampsiteException(ErrorMessageEnum.MAX_1_MONTH_BEFORE_DAY_ARRIVAL);
        }
    }

    public boolean isDataRangeFree(final Reservation reservation) {
        List<LocalDate> reservationPeriodList = getLocalDates(reservation.getArrivalAt(), reservation.getDepartureAt());
        List<LocalDate> availablePeriodList = getAvailableDaysPerPeriod(reservation.getArrivalAt(), reservation.getDepartureAt());

        return reservationPeriodList.size() == availablePeriodList.size();
    }

    public List<LocalDate> getAvailableDaysPerPeriod(final LocalDate beginDate, final LocalDate endDate) {
        List<Reservation> reservationList = reservationRepository.findByArrivalAtBetweenOrDepartureAtBetween(beginDate, endDate, beginDate, endDate);
        Set<LocalDate> unavailableDates = new LinkedHashSet<>();
        reservationList.forEach(reservation -> {
            unavailableDates.addAll(getLocalDates(reservation.getArrivalAt(), reservation.getDepartureAt()));
        });

        List<LocalDate> availableDates = new ArrayList<>(getLocalDates(beginDate, endDate));
        availableDates.removeAll(unavailableDates);

        return availableDates;
    }

    private List<LocalDate> getLocalDates(final LocalDate beginDate, final LocalDate endDate) {
        return Stream.iterate(beginDate, date -> date.plusDays(1))
                .limit(ChronoUnit.DAYS.between(beginDate, endDate))
                .collect(Collectors.toList());
    }
}
