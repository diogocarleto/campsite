package com.upgrade.campsite.exception;

public class CampsiteException extends RuntimeException {

    private ErrorMessageEnum errorMessageEnum;
    private Object[] params;

    public CampsiteException(String message, Throwable cause, ErrorMessageEnum errorMessageEnum, Object... params) {
        super(message, cause);
        this.errorMessageEnum = errorMessageEnum;
        this.params = params;
    }

    public CampsiteException(ErrorMessageEnum errorMessageEnum, Object... params) {
        super();
        this.errorMessageEnum = errorMessageEnum;
        this.params = params;
    }

    public ErrorMessageEnum getErrorMessageEnum() {
        return errorMessageEnum;
    }

    public void setErrorMessageEnum(ErrorMessageEnum errorMessageEnum) {
        this.errorMessageEnum = errorMessageEnum;
    }

    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }
}
