package com.upgrade.campsite.exception;

import java.io.Serializable;

public class ErrorMessageInfo implements Serializable {

    private Integer code;
    private String message;

    public ErrorMessageInfo() {
    }

    public ErrorMessageInfo(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
