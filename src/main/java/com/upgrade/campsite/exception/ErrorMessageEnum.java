package com.upgrade.campsite.exception;

public enum ErrorMessageEnum {

    CAMPSITE_NOT_AVAILABLE(1000),
    MAX_3_DAYS_HOSTED(1001),
    MIN_1_DAY_HOSTED(1002),
    MIN_1_BEFORE_DAY_ARRIVAL(1003),
    MAX_1_MONTH_BEFORE_DAY_ARRIVAL(1004),
    ;

    public final Integer code;

    ErrorMessageEnum(Integer code) {
        this.code = code;
    }
}
