package com.upgrade.campsite.repository;

import com.upgrade.campsite.model.Reservation;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ReservationRepository extends PagingAndSortingRepository<Reservation, Long> {

    List<Reservation> findByArrivalAtBetweenOrDepartureAtBetween(LocalDate arrivalAt, LocalDate departureAt,
                                                                  LocalDate arrivalAt1, LocalDate departureAt1);
}
