package com.upgrade.campsite.controller;

import com.upgrade.campsite.model.Reservation;
import com.upgrade.campsite.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/reservation")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    @RequestMapping(path = "/availability", method = RequestMethod.GET)
    private List<LocalDate> listAvailability(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate beginDate,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return reservationService.getAvailableDaysPerPeriod(beginDate, endDate);
    }

    @RequestMapping(path = "/reserve", method = RequestMethod.POST)
    public Reservation bookRoom(@RequestBody Reservation reservation) {
        return reservationService.reserve(reservation);
    }

    @RequestMapping(path = "/reserve", method = RequestMethod.PUT)
    public Reservation updateReservation(@RequestBody Reservation reservation) {
        return reservationService.modifyReservation(reservation);
    }
}
